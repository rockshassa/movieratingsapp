//
//  GetUserRatingsListOperation.h
//  MovieRatingsApp
//
//  Created by Niko on 4/7/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetUserRatingOperation : NSOperation

-(id)initWithMovieID:(NSString*)movieID;

@end
