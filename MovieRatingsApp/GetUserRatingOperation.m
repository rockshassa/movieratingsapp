//
//  GetUserRatingsListOperation.m
//  MovieRatingsApp
//
//  Created by Niko on 4/7/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "GetUserRatingOperation.h"
#import "Constants.h"
#import "NGAppDelegate.h"
#define appDelegate ((NGAppDelegate*)[[UIApplication sharedApplication] delegate])

@interface GetUserRatingOperation ()

@property (nonatomic, strong) NSString *movieID;

@end

@implementation GetUserRatingOperation

-(id)initWithMovieID:(NSString *)movieID{
    
    self = [super init];
    if (self){
     
        _movieID = movieID;
    }
    return self;
}

-(void)main{
    
    @autoreleasepool {
        
        NSMutableString *urlString = [NSMutableString stringWithFormat:kRatingAddress,appDelegate.userID];
        [urlString appendFormat:@"/%@", _movieID];
        
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                           timeoutInterval:10];
        [request setValue:kSiteAPIKey forHTTPHeaderField:@"APIKey"];
        [request setHTTPMethod:@"GET"];
        
        NSError *requestError;
        NSURLResponse *urlResponse = nil;
        
        NSData *payload = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
        
        if (requestError || !payload) {
            NSLog(@"Error while performing GetUserRatingsListOperation: %@", requestError.localizedDescription);
//            [self performSelectorOnMainThread:@selector(sendFailureNotification) withObject:nil waitUntilDone:YES];
            return;
        }
        
        NSError *jsonError = nil;
        NSArray *jsonObjects = [NSJSONSerialization JSONObjectWithData:payload options:NSJSONReadingMutableContainers error:&jsonError];
        
        NSLog(@"%@",[jsonObjects description]);
        
        if (!jsonError){
            
//            NSMutableArray *incArray = [[NSMutableArray alloc] initWithCapacity:jsonObjects.count];
            
            for (NSDictionary *dict in jsonObjects){
                
                                
            }
        }
    }
}

@end
