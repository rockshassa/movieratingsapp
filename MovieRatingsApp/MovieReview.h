//
//  MovieReview.h
//  MovieRatingsApp
//
//  Created by Niko on 3/28/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieReview : NSObject

@property (nonatomic, strong) NSString *critic;
@property (nonatomic, strong) NSString *reviewBody;

@end
