//
//  MovieSearchOperation.h
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieSearchOperation : NSOperation

-(id)initWithSearchTerm:(NSString*)searchTerm;
-(id)initWithSearchTerm:(NSString *)searchTerm Year:(NSString*)year;

@end
