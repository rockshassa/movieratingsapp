//
//  MovieSearchOperation.m
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//
#import "MovieSearchOperation.h"
#import "Constants.h"
#import "NGAppDelegate.h"
#import "SearchResult.h"
#import "RatingProvider.h"
#import "SuggestedTitle.h"
#define appDelegate ((NGAppDelegate*)[[UIApplication sharedApplication] delegate])

@interface MovieSearchOperation ()

@property (nonatomic, strong) NSString *searchTerm;
@property (nonatomic, strong) NSString *year;

@end

@implementation MovieSearchOperation

-(void)sendSuccessNotification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSearchSuccessNotificaiton object:nil];
}

-(void)sendFailureNotification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSearchFailureNotification object:nil];
}

-(id)initWithSearchTerm:(NSString*)searchTerm{
    return [self initWithSearchTerm:searchTerm Year:nil];
}

-(id)initWithSearchTerm:(NSString *)searchTerm Year:(NSString*)year{
    
    self = [super init];
    if (self) {
        _searchTerm = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                             NULL,
                                                                                             (CFStringRef)searchTerm,
                                                                                             NULL,
                                                                                             (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                             kCFStringEncodingUTF8 ));
        _year = year;
    }
    return self;
    
}

//JSON Keys
static NSString *const kSearchResultTitle = @"Title";
static NSString *const kSearchResultYear = @"Year";
static NSString *const kSearchResultRatingProviders = @"RatingProviders";
static NSString *const kRatingProviderDisplayNameKey = @"DisplayName";
static NSString *const kRatingProviderMaxRatingKey = @"MaxRating";
static NSString *const kRatingProviderRatingKey = @"Rating";
static NSString *const kRatingProviderLogoURL = @"LogoUrl";
static NSString *const kRatingProviderStatusKey = @"Status";
static NSString *const kSearchResultImagePath = @"ImagePath";
static NSString *const kSearchResultSuggestedTitles = @"SuggestedTitles";
static NSString *const KSuggestedTitleNameKey = @"Title";
static NSString *const kSuggestedTitleYearKey = @"Year";
static NSString *const kMovieID = @"ID";

-(void)main{
    
    @autoreleasepool {
        
        NSMutableString *urlString = [NSMutableString stringWithFormat:kSearchGetAddress,_searchTerm];
        if (_year) {
            [urlString appendFormat:@"/%@",_year];
        }
        
//        NSLog(@"%@",urlString);
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                           timeoutInterval:10];
        [request setValue:kSiteAPIKey forHTTPHeaderField:@"APIKey"];
        [request setHTTPMethod:@"GET"];
        
        NSError *requestError;
        NSURLResponse *urlResponse = nil;
        
        NSData *payload = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
        
        if (requestError || !payload) {
            NSLog(@"Error while performing movie search operation: %@", requestError.localizedDescription);
            [self performSelectorOnMainThread:@selector(sendFailureNotification) withObject:nil waitUntilDone:YES];
            return;
        }
        
        NSError *jsonError = nil;
        NSDictionary *jsonObjects = [NSJSONSerialization JSONObjectWithData:payload options:NSJSONReadingMutableContainers error:&jsonError];
        
//        NSLog(@"%@",[jsonObjects description]);
        
        if (!jsonError) {
            
            SearchResult *searchResults = [[SearchResult alloc] init];
            searchResults.title = [jsonObjects objectForKey:kSearchResultTitle];
            searchResults.year = [jsonObjects objectForKey:kSearchResultYear];
            searchResults.imagePath = [jsonObjects objectForKey:kSearchResultImagePath];
            searchResults.movieID = [jsonObjects objectForKey:kMovieID];
            
            NSArray *ratingProviders = [jsonObjects objectForKey:kSearchResultRatingProviders];
            NSMutableArray *providers = [[NSMutableArray alloc] initWithCapacity:ratingProviders.count];
            for (NSDictionary *dict in ratingProviders) {
                
                RatingProvider *rp = [[RatingProvider alloc] init];
                rp.displayName = [dict objectForKey:kRatingProviderDisplayNameKey];
                rp.rating = [dict objectForKey:kRatingProviderRatingKey];
                rp.maxPossibleRating = [dict objectForKey:kRatingProviderMaxRatingKey];
                rp.providerLogoURLString = [dict objectForKey:kRatingProviderLogoURL];
                rp.hasRating = [(NSString*)[dict objectForKey:kRatingProviderStatusKey] boolValue];
                [rp fetchImage];
                
                if ([rp.displayName isEqualToString:@"Rotten Tomatoes"]) {
                    searchResults.rottenTomatoesID = [dict objectForKey:kMovieID];
                }
                
                if (rp.rating.floatValue >= 0)[providers addObject:rp];
            }
            searchResults.ratingProviders = [NSArray arrayWithArray:providers];
            
            NSArray *suggestedTitles = [jsonObjects objectForKey:kSearchResultSuggestedTitles];
            NSMutableArray *incSuggested = [[NSMutableArray alloc] initWithCapacity:suggestedTitles.count];
            for (NSDictionary *dict in suggestedTitles) {
                SuggestedTitle *st = [[SuggestedTitle alloc] init];
                st.title = [dict objectForKey:KSuggestedTitleNameKey];
                st.year = [dict objectForKey:kSuggestedTitleYearKey];
                [incSuggested addObject:st];
            }
            searchResults.suggestedTitles = [NSArray arrayWithArray:incSuggested];
            
//            NSLog(@"%@", searchResults.description);
            
            [searchResults downloadImage];
            
            appDelegate.searchVC.currentResult = searchResults;
            
            [self performSelectorOnMainThread:@selector(sendSuccessNotification) withObject:nil waitUntilDone:YES];
            
        } else {
            
            NSLog(@"Error while parsing JSON: %@", jsonError.localizedDescription);
            NSLog(@"%@",[[NSString alloc] initWithData:payload encoding:NSStringEncodingConversionAllowLossy]);
            
            [self performSelectorOnMainThread:@selector(sendFailureNotification) withObject:nil waitUntilDone:YES];
        }
    }
}

@end