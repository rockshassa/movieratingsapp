//
//  NGAppDelegate.h
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchResult.h"
#import "SearchViewController.h"
#import <FacebookSDK/FacebookSDK.h>


@interface NGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSOperationQueue *operationQueue;
@property (strong, nonatomic) SearchViewController *searchVC;
@property (strong, nonatomic) NSMutableDictionary *ratingProviderLogos;
@property (strong, nonatomic, readonly) id<FBGraphUser> fbUser;
@property (strong, nonatomic, readonly) NSString *userID;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

//Facebook SDK
-(void)openSession;

@end