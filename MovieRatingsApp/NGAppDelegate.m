//
//  NGAppDelegate.m
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import "NGAppDelegate.h"
#import <CoreData/CoreData.h>
#import "UserRatings.h"
#import "GetUserRatingOperation.h"
#import "ViewHelpers.h"

@implementation NGAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    _operationQueue = [[NSOperationQueue alloc] init];
    [self loadLogoDict];
    [self getUserID];
    
    _searchVC = [[SearchViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_searchVC];
    
    [self.window setRootViewController:nav];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    //check for existing facebook session
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        // Yes, so just open the session (this won't display any UX).
        [self openSession];
    }
    
//    [self doDatabaseShit];
    [self doAppearanceShit];
    
    return YES;
}

//put anything that touches the appearance proxy here.
-(void)doAppearanceShit{
    NSDictionary* attrs = @{
                            UITextAttributeFont: [UIFont fontWithName:HELVETICA_NEUE_LIGHT size:20],
                            UITextAttributeTextColor : [UIColor blackColor]
                            };
    [[UINavigationBar appearance] setTitleTextAttributes:attrs];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:HELVETICA_NEUE_LIGHT size:16]];
}

-(void)doDatabaseShit{
    
    UserRatings *ur = [NSEntityDescription insertNewObjectForEntityForName:@"UserRatings" inManagedObjectContext:[self managedObjectContext]];
    ur.imdbID = @"imdb id goes here";
    
    NSString *uid = @"nicks user ID";
    
    ur.userID = uid;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"UserRatings"];
    fetch.fetchBatchSize = 10;
    fetch.fetchLimit = 10;
    fetch.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"userID" ascending:YES]];
    fetch.predicate = [NSPredicate predicateWithFormat:@"userID = %@", uid];
    
    NSError *error;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetch error:&error];
    
    for (UserRatings *u in result) {
        NSLog(@"%@ %@", u.userID, u.imdbID);
    }
}

-(void)saveContext{
    NSLog(@"called savecontext");
}

//Method writes a string to a text file
-(void)saveUserID{
    
    if (!_userID) {
        return;
    }
    
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/userID",
                          documentsDirectory];
    //create content - four lines of text
    NSString *content = _userID;
    //save content to the documents directory
    [content writeToFile:fileName
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
    NSLog(@"wrote user id to disk: %@",_userID);
}


//Method retrieves content from documents directory and
//displays it in an alert
-(void)getUserID{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/userID",
                          documentsDirectory];
    _userID = [[NSString alloc] initWithContentsOfFile:fileName
                                                    usedEncoding:nil
                                                           error:nil];
}

-(void)saveLogoDict{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *filePath = [basePath stringByAppendingPathComponent:@"logodict.plist"];
    
    // _ratingProviderLogos is a NSMutableDictionary
    NSMutableDictionary *logoImgs = [[NSMutableDictionary alloc] initWithCapacity:_ratingProviderLogos.count];
    for (NSString *key in _ratingProviderLogos) {
        UIImage *img = [_ratingProviderLogos objectForKey:key];
        NSData *imgData = [NSData dataWithData:UIImagePNGRepresentation(img)];
        [logoImgs setObject:imgData forKey:key];
    }
    
    NSString *error;
    NSData *logoData = [NSPropertyListSerialization dataFromPropertyList:logoImgs  format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
    
    if(logoData) {
        [logoData writeToFile:filePath atomically:YES];
        NSLog(@"wrote logodata to disk");
    } else {
        NSLog(@"%@",error);
    }
}

-(void)loadLogoDict{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *filePath = [basePath stringByAppendingPathComponent:@"logodict.plist"];
    
    NSMutableDictionary *logos = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
    
    _ratingProviderLogos = [[NSMutableDictionary alloc] init];
    
    if (logos) {
        for (NSString *key in logos) {
            
            UIImage *img = [UIImage imageWithData:[logos objectForKey:key]];
            [_ratingProviderLogos setObject:img forKey:key];
        }
    }
}

#pragma mark - Facebook session

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: {
            NSLog(@"Facebook Session opened");
            
            [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id<FBGraphUser> user, NSError *error)
             {
//                 NSLog(@"facebook result: %@", result);
                 if (!error) {
                     _fbUser = user;
                     
                     _userID = self.fbUser.id;
                     
                     if (_userID) {
                         [self saveUserID];
                     }
                     
                     NSString *userInfo = @"";
                     
                     // Example: typed access (name)
                     // - no special permissions required
                     userInfo = [userInfo
                                 stringByAppendingString:
                                 [NSString stringWithFormat:@"Name: %@\n\n",
                                  user.name]];
                     
                     // Example: typed access, (birthday)
                     // - requires user_birthday permission
                     userInfo = [userInfo
                                 stringByAppendingString:
                                 [NSString stringWithFormat:@"Birthday: %@\n\n",
                                  user.birthday]];
                     
                     // Example: partially typed access, to location field,
                     // name key (location)
                     // - requires user_location permission
                     userInfo = [userInfo
                                 stringByAppendingString:
                                 [NSString stringWithFormat:@"Location: %@\n\n",
                                  [user.location objectForKey:@"name"]]];
                     
                     // Example: access via key (locale)
                     // - no special permissions required
                     userInfo = [userInfo
                                 stringByAppendingString:
                                 [NSString stringWithFormat:@"Locale: %@\n\n",
                                  [user objectForKey:@"locale"]]];
                     
                     // Example: access via key for array (languages)
                     // - requires user_likes permission
                     if ([user objectForKey:@"languages"]) {
                         NSArray *languages = [user objectForKey:@"languages"];
                         NSMutableArray *languageNames = [[NSMutableArray alloc] init];
                         for (int i = 0; i < [languages count]; i++) {
                             [languageNames addObject:[[languages
                                                        objectAtIndex:i]
                                                       objectForKey:@"name"]];
                         }
                         userInfo = [userInfo
                                     stringByAppendingString:
                                     [NSString stringWithFormat:@"Languages: %@\n\n",
                                      languageNames]];
                     }
                     
//                     NSLog(@"%@USER ID:%@",userInfo, user.id);
                     
                 } else {
                     NSLog(@"facebook error: %@", error.localizedDescription);
                 }
             }];
            
        }
            break;
        case FBSessionStateClosed:
            NSLog(@"Facebook Session closed");
            break;
        case FBSessionStateClosedLoginFailed:
            // Once the user has logged in, we want them to
            // be looking at the root view.
            NSLog(@"Facebook login failed");
            
            [FBSession.activeSession closeAndClearTokenInformation];
            
            break;
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)openSession
{
    [FBSession openActiveSessionWithReadPermissions:nil
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         [self sessionStateChanged:session state:state error:error];
     }];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

-(void)applicationDidBecomeActive:(UIApplication *)application{
    // We need to properly handle activation of the application with regards to Facebook Login
    // (e.g., returning from iOS 6.0 Login Dialog or from fast app switching).
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self saveLogoDict];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"cd" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"cd.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
