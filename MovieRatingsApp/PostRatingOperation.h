//
//  PostRatingOperation.h
//  MovieRatingsApp
//
//  Created by Niko on 3/17/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostRatingOperation : NSOperation

-(id)initWithMovieID:(NSString*)movieID rating:(NSString*)rating;

@end
