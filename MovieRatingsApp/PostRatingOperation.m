//
//  PostRatingOperation.m
//  MovieRatingsApp
//
//  Created by Niko on 3/17/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "PostRatingOperation.h"
#import "Constants.h"
#import "NGAppDelegate.h"

@interface PostRatingOperation ()

@property (nonatomic, strong) NSString *movieID;
@property (nonatomic, strong) NSString *rating;

@end

@implementation PostRatingOperation

-(id)initWithMovieID:(NSString*)movieID rating:(NSString*)rating{
    
    self = [super init];
    if (self) {
    
        _movieID = movieID;
        _rating = rating;
    
    }
    return self;
}

-(void)sendSuccessNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPostRatingSuccessNotification object:nil];
}

-(void)sendFailureNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPostRatingFailureNotification object:nil];
}

-(void)main{
    
    NGAppDelegate *appDelegate = (NGAppDelegate*)[UIApplication sharedApplication].delegate;
    
    //nil check before creating postDict
    if (!(appDelegate.userID && _movieID && _rating)) {
        return;
    }
    
    NSURL *url = [NSURL URLWithString:kRatingAddress];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    [request setValue:kSiteAPIKey forHTTPHeaderField:@"APIKey"];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *postDict = @{@"UserID": appDelegate.userID,
                               @"IMDBID": _movieID,
                               @"Rating": _rating};
    
    NSError *jsonError;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:postDict options:0 error:&jsonError];
    request.HTTPBody = postData;
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    
    if (requestError) {
        NSLog(@"Error while performing post rating operation: %@", requestError.localizedDescription);
        [self performSelectorOnMainThread:@selector(sendFailureNotification) withObject:nil waitUntilDone:YES];
    } else {
        [self performSelectorOnMainThread:@selector(sendSuccessNotification) withObject:nil waitUntilDone:YES];
    }
}


@end
