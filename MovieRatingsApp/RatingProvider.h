//
//  RatingProvider.h
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RatingProvider : NSObject <NSCoding>

@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *maxPossibleRating;
@property (nonatomic, strong) NSString *providerLogoURLString;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign, readonly) BOOL hasImage;
@property (nonatomic, assign) BOOL hasRating;

+(NSArray*)dummyArrayWithLength:(int)length;
-(void)fetchImage;

@end
