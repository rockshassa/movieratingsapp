//
//  RatingProvider.m
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "RatingProvider.h"
#import "Constants.h"
#import "NGAppDelegate.h"
#define appDelegate ((NGAppDelegate*)[[UIApplication sharedApplication] delegate])

@interface RatingProvider ()

@property (nonatomic, assign) BOOL isDownloadingImage;

@end


@implementation RatingProvider

+(NSArray*)dummyArrayWithLength:(int)length{
    
    NSMutableArray *incArray = [[NSMutableArray alloc] initWithCapacity:length];
    
    for (int i = 0; i < length; i++) {
        
        RatingProvider *rp = [[RatingProvider alloc] init];
        rp.displayName = [NSString stringWithFormat:@"Dummy Provider %i",i];
        rp.rating = [NSString stringWithFormat:@"%i", i];
        rp.maxPossibleRating = [NSString stringWithFormat:@"%i", i];
        
        [incArray addObject:rp];
    }
    return incArray;
}

-(BOOL)hasImage{
    return (_image != nil);
}

-(NSString*)description{
    
    return [NSString stringWithFormat:@"\rdisplayName: %@\rrating: %@ of %@\r: rpath %@",_displayName, _rating, _maxPossibleRating, _providerLogoURLString];
}

-(void)fetchImage{
    
    //look for a cached version of the logo image first
    _image = [appDelegate.ratingProviderLogos objectForKey:_displayName];
    
    if (!_image && !_isDownloadingImage){
        
        _isDownloadingImage = YES;
        
        NSString *urlString = [NSString stringWithFormat:kSiteRoot, _providerLogoURLString];
        
        _image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
        
        if (_image){
            
            [appDelegate.ratingProviderLogos setObject:_image forKey:_displayName];
            [self performSelectorOnMainThread:@selector(sendImageSuccessNotification) withObject:nil waitUntilDone:NO];
        }
        
        _isDownloadingImage = NO;
    }
}

-(void)sendImageSuccessNotification{
    
    NSLog(@"got image for %@", _displayName);
    [[NSNotificationCenter defaultCenter] postNotificationName:kProviderLogoDownloadSuccess object:self];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        
        _displayName = [aDecoder decodeObjectForKey:@"displayName"];
        _image = [aDecoder decodeObjectForKey:@"image"];
        
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:_displayName forKey:@"displayName"];
    [aCoder encodeObject:_image forKey:@"image"];
    
}

@end
