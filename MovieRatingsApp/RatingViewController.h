//
//  RatingViewController.h
//  MovieRatingsApp
//
//  Created by Niko on 3/17/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarRatingControl.h"

@class SearchResult;

@interface RatingViewController : UIViewController <UITextFieldDelegate>

-(id)initWithResult:(SearchResult*)result;

@end
