//
//  RatingViewController.m
//  MovieRatingsApp
//
//  Created by Niko on 3/17/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "RatingViewController.h"
#import "SearchResult.h"
#import <FacebookSDK/FacebookSDK.h>
#import "NGAppDelegate.h"
#import <Social/Social.h>
#import "PostRatingOperation.h"
#import "SVProgressHUD.h"
#import "Constants.h"

#define appDelegate ((NGAppDelegate*)[[UIApplication sharedApplication] delegate])

@interface RatingViewController ()
@property (strong, nonatomic) SearchResult *currentResult;
@property (strong, nonatomic) UIButton *rateAndShareButton;
@property (strong, nonatomic) UIButton *shareButton;
@property (strong, nonatomic) UIImageView *movieImg;
@property (strong, nonatomic) UITextField *ratingField;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) StarRatingControl *starRatingControl;
@end

@implementation RatingViewController

- (id)initWithResult:(SearchResult *)result
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        
        _currentResult = result;
        
        UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissView)];
        self.navigationItem.rightBarButtonItem = rightButton;
        
    }
    return self;
}

-(void)loadView{
    
    UIView *incView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
    incView.backgroundColor = [UIColor cyanColor];
    
    _rateAndShareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _rateAndShareButton.frame = CGRectMake(0, 0, 300, 50);
    [_rateAndShareButton setTitle:@"Rate and Share" forState:UIControlStateNormal];
    [_rateAndShareButton addTarget:self action:@selector(tappedRateAndShare:) forControlEvents:UIControlEventTouchUpInside];
    [incView addSubview:_rateAndShareButton];
    
    _shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _shareButton.frame = CGRectMake(0, 0, 300, 50);
    [_shareButton setTitle:@"rate only :(" forState:UIControlStateNormal];
    [_shareButton addTarget:self action:@selector(tappedRate:) forControlEvents:UIControlEventTouchUpInside];
    [incView addSubview:_shareButton];
    
    if (_currentResult.image) {
        _movieImg = [[UIImageView alloc] initWithImage:_currentResult.image];
        [incView addSubview:_movieImg];
    }
    
    CGRect labelFrame = CGRectMake(_movieImg.frame.size.width+_movieImg.frame.origin.x+10, _movieImg.frame.origin.y, 0, 1);
    labelFrame.size.width = incView.frame.size.width-labelFrame.origin.x-10;
    
    _titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    _titleLabel.text = [NSString stringWithFormat:@"%@",_currentResult.title];
    _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:30];
    _titleLabel.numberOfLines = 0;
    [_titleLabel sizeToFit];
    
    [incView addSubview:_titleLabel];
    
    _ratingField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 300, 50)];
    _ratingField.delegate = self;
    _ratingField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _ratingField.returnKeyType = UIReturnKeyDone;
    
//    [incView addSubview:_ratingField];
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, incView.frame.size.width, 44)];
    toolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ,[[UIBarButtonItem alloc] initWithTitle:@"Dismiss" style:UIBarButtonItemStyleBordered target:self action:@selector(tappedDismiss:)]];
    toolbar.barStyle = UIBarStyleBlack;
    toolbar.translucent = YES;
//    _ratingField.inputAccessoryView = toolbar;
    
    _starRatingControl = [[StarRatingControl alloc] initWithFrame:CGRectMake(0, 0, incView.frame.size.width, 60) andStars:5];
    _starRatingControl.rating = 0;
    [incView addSubview:_starRatingControl];
    
    self.view = incView;
}

-(void)viewWillLayoutSubviews{
    
    _rateAndShareButton.center = CGPointMake(self.view.frame.size.width/2, 300);
    _starRatingControl.center = CGPointMake(self.view.frame.size.width/2, 225);
    _shareButton.center = CGPointMake(self.view.frame.size.width/2, 360);
    
}

#pragma mark - Send Rating

-(void)sendRating{
    
    if (_currentResult.movieID && [NSString stringWithFormat:@"%i", _starRatingControl.rating]) {
        PostRatingOperation *postOp = [[PostRatingOperation alloc] initWithMovieID:_currentResult.movieID
                                                                            rating:[NSString stringWithFormat:@"%i", _starRatingControl.rating]];
        [appDelegate.operationQueue addOperation:postOp];
    } else {
        //handle missing ID
    }
}

#pragma mark - Button Targets
-(void)tappedRateAndShare:(id)sender{
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   kFacebookAppID, @"app_id",
                                   @"http://apps.facebook.com/movieocd/", @"link",
                                   _currentResult.imagePath, @"picture",
                                   @"MovieOCD", @"name",
                                   [NSString stringWithFormat:@"%@ rated %@ (%@)",appDelegate.fbUser.first_name,_currentResult.title, _currentResult.year], @"caption",
                                   [NSString stringWithFormat:@"%i out of %i stars!",_starRatingControl.rating, _starRatingControl.numberOfStars], @"description",
                                   nil];
    
    [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession]
                                           parameters:params
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error){
                                              
                                                  [self performSelectorInBackground:@selector(sendRating) withObject:nil];
                                                  
                                                  if (result == FBWebDialogResultDialogCompleted) {
                                                      [self dismissView];
                                                  }
                                              }];
}

-(void)tappedRate:(id)sender{
    
    [self performSelectorInBackground:@selector(sendRating) withObject:nil];
    [self dismissView];
}

#pragma mark - Text Field Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
//    [self tappedSearch];
    return YES;
}

-(void)tappedDismiss:(id)sender{
    [_ratingField resignFirstResponder];
}


-(void)dismissView{
    [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
