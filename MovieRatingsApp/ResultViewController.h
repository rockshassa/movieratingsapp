//
//  ResultViewController.h
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchViewController.h"
@class SearchResult;

@interface ResultViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

-(id)initWithResults:(SearchResult*)results Delegate:(id<SearchDelegate>)delegate;

@end
