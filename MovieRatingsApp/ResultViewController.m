//
//  ResultViewController.m
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "ResultViewController.h"
#import "SearchResult.h"
#import "Constants.h"
#import "RatingProvider.h"
#import "SuggestedTitle.h"
#import <FacebookSDK/FacebookSDK.h>
#import "NGAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "RatingViewController.h"
#import "ReviewViewController.h"
#import "ReviewDownloadOperation.h"
#import "SVProgressHUD.h"
#import "GetUserRatingOperation.h"
#import "ViewHelpers.h"

#define appDelegate ((NGAppDelegate*)[[UIApplication sharedApplication] delegate])

#define HEADER_HEIGHT 200
#define RATINGS_TABLE_SECTION 0
#define SUGGESTIONS_TABLE_SECTION 1

@interface ResultViewController ()

@property (nonatomic, strong) SearchResult *results;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) id <SearchDelegate> delegate;

@end

@implementation ResultViewController

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(id)initWithResults:(SearchResult*)results Delegate:(id<SearchDelegate>)delegate{
    
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        self.title = @"Results";
        _results = results;
        
        NSMutableArray *incData = [[NSMutableArray alloc] init];
        for (RatingProvider *rp in _results.ratingProviders) {
            if (rp.hasRating)[incData addObject:rp];
        }
        _dataSource = incData;
        _delegate = delegate;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoImgDownloaded) name:kProviderLogoDownloadSuccess object:nil];
        
    }
    return self;
}

-(void)loadView{
    
    UIView *incView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
    incView.backgroundColor = [UIColor greenColor];
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                 target:self
                                                                                 action:@selector(tappedShare:)];
    self.navigationItem.rightBarButtonItem = shareButton;
    
    _tableView = [[UITableView alloc] initWithFrame:incView.frame];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.tableHeaderView = [self buildTableHeader];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [incView addSubview:_tableView];
    
    self.view = incView;
    
}

-(UIView*)buildTableHeader{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,10,_tableView.frame.size.width,0)];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:_results.image];
    [headerView addSubview:imgView];
    
    CGRect labelFrame = CGRectMake(imgView.frame.size.width+imgView.frame.origin.x+10, imgView.frame.origin.y, 0, 1);
    labelFrame.size.width = headerView.frame.size.width-labelFrame.origin.x-10;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    titleLabel.text = [NSString stringWithFormat:@"%@ (%@)",_results.title, _results.year];
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:30];
    titleLabel.numberOfLines = 0;
    [titleLabel sizeToFit];
    
    [headerView addSubview:titleLabel];
    
    if (bottomOfView(titleLabel) >= bottomOfView(imgView)) {
        headerView.frame = CGRectMake(0, 0, _tableView.frame.size.width, bottomOfView(titleLabel));
    } else {
        headerView.frame = CGRectMake(0, 0, _tableView.frame.size.width, bottomOfView(imgView));
    }
    
    return headerView;
}

-(void)logoImgDownloaded{
    [_tableView reloadData];
}

#pragma mark - Review Downloads

-(void)reviewDownloadSucceeded{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (_results.reviews.count == 0) {
        [SVProgressHUD showErrorWithStatus:@"No reviews posted"];
    } else {
    
        [SVProgressHUD dismiss];
        
        ReviewViewController *rvc = [[ReviewViewController alloc] initWithResult:_results];
        [self.navigationController pushViewController:rvc animated:YES];
    }
    
}

-(void)reviewDownloadFailed{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [SVProgressHUD showErrorWithStatus:@"Reviews Unavailable"];
}

#pragma mark - Button Targets

-(void)tappedShare:(id)sender{
    
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        // To-do, show logged in view
    } else {
        // No, display the login page.
        [self performLogin];
    }
}

-(void)tappedRate:(id)sender{
    
    RatingViewController *rvc = [[RatingViewController alloc] initWithResult:_results];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:rvc];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

#pragma mark - Facebook handling

-(void)performLogin{
    [appDelegate openSession];
}

#pragma mark - UITableView Data Source

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UILabel *label;
    
    if (section == RATINGS_TABLE_SECTION || section == SUGGESTIONS_TABLE_SECTION) {
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
        [label applySectionHeaderStyle];
        
        switch (section) {
            case RATINGS_TABLE_SECTION:
                label.text = @"  Ratings";
                break;
            case SUGGESTIONS_TABLE_SECTION:
                label.text =  @"  Did you mean...";
                break;
        }
    }
    
    return label;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    int sections = 1;
    if (_results.suggestedTitles.count > 0) sections++;
    return sections;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    switch (section) {
        case RATINGS_TABLE_SECTION:
            return _dataSource.count;
            break;
        case SUGGESTIONS_TABLE_SECTION:
            return _results.suggestedTitles.count;
            break;
        default:
            return 0;
            break;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"whatever"];
    
    if (!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"whatever"];
    }
    
    switch (indexPath.section) {
        case RATINGS_TABLE_SECTION:{
            
            RatingProvider *rp = [_dataSource objectAtIndex:indexPath.row];
            
            if (!rp.image) {
                [rp performSelectorInBackground:@selector(fetchImage) withObject:nil];
            } else {
                [cell.imageView setImage:rp.image];
            }
            
            cell.textLabel.text = rp.displayName;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Rating: %@ of %@", rp.rating, rp.maxPossibleRating];
            
            //this is to show reviews from Rotten Tomatoes
            if ([rp.displayName isEqualToString:@"Rotten Tomatoes"] && _results.rottenTomatoesID) {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            
            break;
        }
        case SUGGESTIONS_TABLE_SECTION:{
            
            cell.textLabel.text = [(SuggestedTitle*)[_results.suggestedTitles objectAtIndex:indexPath.row] title] ;
            
            break;
        }
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case RATINGS_TABLE_SECTION:{
            
            RatingProvider *rp = [_dataSource objectAtIndex:indexPath.row];
            //this is to show reviews from Rotten Tomatoes
            if ([rp.displayName isEqualToString:@"Rotten Tomatoes"] && _results.rottenTomatoesID) {
                
                if (_results.reviews.count == 0){
                
                [SVProgressHUD showWithStatus:@"Fetching"];
                
                NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
                [nc addObserver:self selector:@selector(reviewDownloadSucceeded) name:kReviewDownloadSuccessNotification object:nil];
                [nc addObserver:self selector:@selector(reviewDownloadFailed) name:kReviewDownloadFailureNotification object:nil];
                
                ReviewDownloadOperation *reviewOp = [[ReviewDownloadOperation alloc] initWithResult:_results];
                    [appDelegate.operationQueue addOperation:reviewOp];
                } else {
                    [self reviewDownloadSucceeded];
                }
            }
            
            break;
        }
        case SUGGESTIONS_TABLE_SECTION:{
            
            SuggestedTitle *st = [_results.suggestedTitles objectAtIndex:indexPath.row];
            
            [self.delegate initiateSearchWithTitle:st.title Year:st.year];
            [self.navigationController popViewControllerAnimated:YES];
            
            break;
        }
        default:
            break;
    }
    
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    switch (section) {
        case RATINGS_TABLE_SECTION:{
            UIView *rateView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, [self tableView:tableView heightForFooterInSection:section])];
            rateView.backgroundColor = [UIColor whiteColor];
            
            UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            shareBtn.frame = rateView.frame;
            [shareBtn setImage:[UIImage imageNamed:@"rate_and_share"] forState:UIControlStateNormal];
            [shareBtn sizeToFit];
            shareBtn.center = rateView.center;
            [shareBtn addTarget:self
                         action:@selector(tappedRate:)
               forControlEvents:UIControlEventTouchUpInside];
            
            [shareBtn.layer setMasksToBounds:YES];
            [shareBtn.layer setCornerRadius:5];
            
            [rateView addSubview:shareBtn];
            
            return rateView;
            break;
        }
        default:
            return nil;
            break;
    }
}

#pragma mark - UITableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    switch (section) {
        case RATINGS_TABLE_SECTION:
            return 60;
            break;
            
        default:
            return 0;
            break;
    }
}

@end
