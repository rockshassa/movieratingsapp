//
//  ReviewDownloadOperation.h
//  MovieRatingsApp
//
//  Created by Niko on 3/28/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SearchResult;
@interface ReviewDownloadOperation : NSOperation

-(id)initWithResult:(SearchResult*)result;

@end
