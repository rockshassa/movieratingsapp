//
//  ReviewDownloadOperation.m
//  MovieRatingsApp
//
//  Created by Niko on 3/28/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "ReviewDownloadOperation.h"
#import "Constants.h"
#import "MovieReview.h"
#import "SearchResult.h"

@interface ReviewDownloadOperation ()

@property (nonatomic, strong) SearchResult *result;

@end

@implementation ReviewDownloadOperation

-(id)initWithResult:(SearchResult *)result{
    
    self = [super init];
    if (self){
        _result = result;
    }
    return self;
}

-(void)sendSuccessNotification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kReviewDownloadSuccessNotification object:nil];
}

-(void)sendFailureNotification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kReviewDownloadFailureNotification object:nil];
}

static NSString *const kCriticKey = @"Critic";
static NSString *const kReviewKey = @"Review";

-(void)main{
    
    @autoreleasepool {
        
        NSMutableString *urlString = [NSMutableString stringWithFormat:kReviewAddress,_result.rottenTomatoesID];
        
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                           timeoutInterval:10];
        [request setValue:kSiteAPIKey forHTTPHeaderField:@"APIKey"];
        [request setHTTPMethod:@"GET"];
        
        NSError *requestError;
        NSURLResponse *urlResponse = nil;
        
        NSData *payload = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
        
        if (requestError || !payload) {
            NSLog(@"Error while performing review download operation: %@", requestError.localizedDescription);
            [self performSelectorOnMainThread:@selector(sendFailureNotification) withObject:nil waitUntilDone:YES];
            return;
        }
        
        NSError *jsonError = nil;
        NSArray *jsonObjects = [NSJSONSerialization JSONObjectWithData:payload options:NSJSONReadingMutableContainers error:&jsonError];
        
//        NSLog(@"%@",[jsonObjects description]);
        
        if (!jsonError){
            
            NSMutableArray *incArray = [[NSMutableArray alloc] initWithCapacity:jsonObjects.count];
            
            for (NSDictionary *dict in jsonObjects){
                
                MovieReview *mr = [[MovieReview alloc] init];
                mr.critic = [dict objectForKey:kCriticKey];
                mr.reviewBody = [dict objectForKey:kReviewKey];
                [incArray addObject:mr];
                
            }
            
            _result.reviews = incArray;
            
            [self performSelectorOnMainThread:@selector(sendSuccessNotification) withObject:nil waitUntilDone:YES];
            
        } else {
            NSLog(@"Error while parsing JSON: %@", jsonError.localizedDescription);
            NSLog(@"%@",[[NSString alloc] initWithData:payload encoding:NSStringEncodingConversionAllowLossy]);
            [self performSelectorOnMainThread:@selector(sendFailureNotification) withObject:nil waitUntilDone:YES];
        }
    }
}

@end
