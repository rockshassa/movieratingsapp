//
//  ReviewViewController.h
//  MovieRatingsApp
//
//  Created by Niko on 3/28/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchResult;

@interface ReviewViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
-(id)initWithResult:(SearchResult*)result;
@end
