//
//  ReviewViewController.m
//  MovieRatingsApp
//
//  Created by Niko on 3/28/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "ReviewViewController.h"
#import "SearchResult.h"
#import "MovieReview.h"

@interface ReviewViewController ()
@property (nonatomic, strong) SearchResult *result;
@end

@implementation ReviewViewController

- (id)initWithResult:(SearchResult *)result
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        self.title = @"Reviews";
        _result = result;
    }
    return self;
}

-(void)loadView{
    
    UITableView *incTableView = [[UITableView alloc] initWithFrame:self.navigationController.view.bounds style:UITableViewStylePlain];
    incTableView.dataSource = self;
    incTableView.delegate = self;
    
    self.view = incTableView;
    
}

#pragma mark - UITableView Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _result.reviews.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MovieReview *mr = [_result.reviews objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"whatev"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"whatev"];
    }
    
    cell.textLabel.text = mr.critic;
    cell.detailTextLabel.numberOfLines = 4;
    cell.detailTextLabel.text = mr.reviewBody;
    
    return cell;
}

#pragma mark - UITableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

@end
