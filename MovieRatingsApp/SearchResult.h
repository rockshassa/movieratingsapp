//
//  SearchResult.h
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RatingProvider.h"

@interface SearchResult : NSObject

@property (nonatomic, strong) NSArray *ratingProviders;
@property (nonatomic, strong) NSArray *suggestedTitles;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *year;
@property (nonatomic, assign, readonly) CGFloat compositeRating;
@property (nonatomic, strong) NSString *movieID;
@property (nonatomic, strong) NSString *rottenTomatoesID; //used for getting reviews
@property (nonatomic, strong) NSArray *reviews;

+(SearchResult*)dummyResult;

-(void)downloadImage;

@end
