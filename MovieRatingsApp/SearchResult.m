//
//  SearchResult.m
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "SearchResult.h"
#import "Constants.h"
#import "SuggestedTitle.h"

@interface SearchResult ()

@property (nonatomic, assign) BOOL isDownloadingImage;

@end

@implementation SearchResult

+(SearchResult*)dummyResult{
    
    SearchResult *sr = [[SearchResult alloc] init];
    sr.title = @"title";
    sr.ratingProviders = [RatingProvider dummyArrayWithLength:5];
    sr.suggestedTitles = [SuggestedTitle dummyArray];
    return sr;
}

-(NSString*)description{
    
    return [NSString stringWithFormat:@"\rTitle: %@\rImagePath: %@\rRatingProviders: %@\rSuggestedTitles: %@",_title, _imagePath, _ratingProviders.description, _suggestedTitles.description];
}

-(CGFloat)compositeRating{
    
    CGFloat total = 0;
    int i = 0;
    for (RatingProvider *rp in _ratingProviders) {
        total += rp.rating.floatValue / rp.maxPossibleRating.floatValue;
        i++;
    }
    
    if (i){
        return total/i;
    } else {
        return 0;
    }
}


-(void)downloadImage{
    
    if (!_image && !_isDownloadingImage){
        
        _isDownloadingImage = YES;
        
        _image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_imagePath]]];
        
        if (_image){
            [self performSelectorOnMainThread:@selector(sendImageSuccessNotification) withObject:nil waitUntilDone:NO];
        }
        
        _isDownloadingImage = NO;
    }
}

-(void)sendImageSuccessNotification{
    
    NSLog(@"got image for %@", _title);
    [[NSNotificationCenter defaultCenter] postNotificationName:kImageDownloadSuccess object:self];
    
}

@end
