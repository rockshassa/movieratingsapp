//
//  SearchViewController.h
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchResult;

@protocol SearchDelegate <NSObject>

-(void)initiateSearchWithTitle:(NSString*)title Year:(NSString*)year;

@end

@interface SearchViewController : UIViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, SearchDelegate>
@property (nonatomic, strong) SearchResult *currentResult;

@end
