//
//  SearchViewController.m
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SearchViewController.h"
#import "MovieSearchOperation.h"
#import "NGAppDelegate.h"
#import "SVProgressHUD.h"
#import "Constants.h"
#import "SuggestedTitle.h"
#import "ResultViewController.h"
#import "UIImage+Extras.h"
#import "ViewHelpers.h"

#define appDelegate ((NGAppDelegate*)[[UIApplication sharedApplication] delegate])

@interface SearchViewController ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UISearchBar *searchField;
@property (nonatomic, strong) UIButton *searchButton;
@property (nonatomic, strong) NSMutableArray *recentSearches;
@property (nonatomic, strong) UITableView *suggestionTable;
@property (nonatomic, strong) UITableView *recentSearchesTable;

@end

@implementation SearchViewController

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        self.title = @"Movie Search";
        _recentSearches = [[NSMutableArray alloc] initWithCapacity:5];
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(keyboardWillShow:)
         name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(keyboardWillHide:)
         name:UIKeyboardWillHideNotification object:nil];
        
//        appDelegate.currentSearchResult = [SearchResult dummyResult];
        
    }
    return self;
}

-(void)loadView{
    
    UIView *incView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
    incView.backgroundColor = [UIColor lightGrayColor];
    
    _searchField = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 300, 50)];
    _searchField.placeholder = @"Type movie name here";
    _searchField.text = @"Pootie Tang";
    _searchField.delegate = self;
    _searchField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, incView.frame.size.width, 44)];
    toolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ,[[UIBarButtonItem alloc] initWithTitle:@"Dismiss" style:UIBarButtonItemStylePlain target:self action:@selector(tappedDismiss:)]];
    toolbar.barStyle = UIBarStyleBlack;
    toolbar.translucent = YES;
    _searchField.inputAccessoryView = toolbar;
    
    [incView addSubview:_searchField];
    
    _searchButton = buttonWithColor([UIColor blueColor]);
    [_searchButton setTitle:@"Search it, my broheim" forState:UIControlStateNormal];
    [_searchButton addTarget:self action:@selector(tappedSearch) forControlEvents:UIControlEventTouchUpInside];
//    [_searchButton addTarget:self action:@selector(searchCompleted) forControlEvents:UIControlEventTouchUpInside];
    
    [incView addSubview:_searchButton];
    
    UIButton *cacheBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cacheBtn setTitle:@"clear cache" forState:UIControlStateNormal];
    [cacheBtn addTarget:self action:@selector(clearCache) forControlEvents:UIControlEventTouchUpInside];
    cacheBtn.frame = CGRectMake(0, 0, 100, 40);
    cacheBtn.center = CGPointMake(incView.frame.size.width/2, 250);
    [incView addSubview:cacheBtn];
    
    self.view = incView;
    
}

-(void)viewWillLayoutSubviews{
    
    [super viewWillLayoutSubviews];
    
    CGRect searchFrame = _searchField.frame;
    searchFrame.origin = CGPointMake(0, bottomOfView(self.navigationController.navigationBar));
    searchFrame.size = CGSizeMake(self.view.frame.size.width, 50);
    _searchField.frame = searchFrame;
    
    _searchButton.frame = CGRectMake(0, bottomOfView(_searchField)+20, 250, 50);
    _searchButton.center = CGPointMake(floorf(self.view.frame.size.width/2), _searchButton.center.y);
    
}

-(void)clearCache{
   
    NSMutableString *urlString = [NSMutableString stringWithFormat:kSiteRoot,@"/movie/clear"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    [request setValue:kSiteAPIKey forHTTPHeaderField:@"APIKey"];
    [request setHTTPMethod:@"GET"];
    
    NSError *requestError;
    NSHTTPURLResponse *urlResponse = nil;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    
    [SVProgressHUD showSuccessWithStatus:@"done"];
    NSLog(@"%i",urlResponse.statusCode);
}

#pragma mark - SearchBar Delegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self tappedSearch];
    [searchBar resignFirstResponder];
}

-(void)tappedDismiss:(id)sender{
    [_searchField resignFirstResponder];
}

#pragma mark - keyboard notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
//    NSDictionary *userInfo = [notification userInfo];
//    CGRect kbFrame = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
//    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//    
//    [self.view.window bringSubviewToFront:_searchField];
//    
//    [UIView animateWithDuration:duration.floatValue
//                     animations:^(void){
//                     
//                         _searchField.frame = CGRectMake(0, 0, _searchField.frame.size.width, _searchField.frame.size.height);
//                     
//                     }
//                     completion:^(BOOL finished){}];
//    
}

- (void)keyboardWillHide:(NSNotification *)notification{
    
//    NSDictionary *userInfo = [notification userInfo];
//    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//    
//    [UIView animateWithDuration:duration.floatValue
//                     animations:^(void){
//                         
//                         _searchField.frame = CGRectMake(0, bottomOfView(self.navigationController.navigationBar), _searchField.frame.size.width, _searchField.frame.size.height);
//                         
//                     }
//                     completion:^(BOOL finished){}];
}


#pragma mark - Search handling

-(void)tappedSearch{
    [self initiateSearchWithTitle:_searchField.text Year:nil];
}

#pragma mark - Search Delegate

-(void)initiateSearchWithTitle:(NSString*)title Year:(NSString*)year{
    
    if (_searchField.isFirstResponder) {
        [_searchField resignFirstResponder];
    }
    
    NSString *searchTerm = [title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (searchTerm.length > 0){
        
        [SVProgressHUD showWithStatus:@"Searching" maskType:SVProgressHUDMaskTypeGradient];
        MovieSearchOperation* searchOp = [[MovieSearchOperation alloc] initWithSearchTerm:searchTerm Year:year];
        [appDelegate.operationQueue addOperation:searchOp];
        
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(searchCompleted) name:kSearchSuccessNotificaiton object:nil];
        [nc addObserver:self selector:@selector(searchFailed) name:kSearchFailureNotification object:nil];
    }
}

#pragma mark - Search Result Handling

-(void)saveRecentSearch:(SearchResult*)recentSearch{

    if (_recentSearches.count > 10) {
        [_recentSearches removeLastObject];
    }
    
    int i = 0;
    for (SearchResult *sv in _recentSearches) {
        if ([sv.title isEqualToString:recentSearch.title] && [sv.year isEqualToString:recentSearch.year]){
            [_recentSearches removeObjectAtIndex:i];
            break;
        }
        i++;
    }
    
    [_recentSearches insertObject:recentSearch atIndex:0];
    
    if (!_recentSearchesTable) {
        [self showRecentSearches];
    }
    
    [_recentSearchesTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    
//    NSLog(@"recentSearches Count %i", _recentSearches.count);
}

-(void)searchCompleted{
    
    [SVProgressHUD dismiss];
    
    if (_currentResult.title.length > 0) {
        [self saveRecentSearch:_currentResult];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self pushSearchResult:_currentResult];
    } else {
        [self showSuggestions];
    }
}

-(void)searchFailed{
    
    [SVProgressHUD showErrorWithStatus:@"Search Failed!"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)showSuggestions{
    
    CGRect tableFrame = CGRectMake(_searchField.frame.origin.x, bottomOfView(_searchField), _searchField.frame.size.width, 250);
    
    _suggestionTable = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
    _suggestionTable.dataSource = self;
    _suggestionTable.delegate = self;
    
    [self.view addSubview:_suggestionTable];
}

-(void)showRecentSearches{
    
    CGRect tableFrame = CGRectMake(0, 200, self.view.frame.size.width, self.view.frame.size.height-200);
    
    _recentSearchesTable = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
    _recentSearchesTable.dataSource = self;
    _recentSearchesTable.delegate = self;
    _recentSearchesTable.backgroundColor = self.view.backgroundColor;
    
    [self.view addSubview:_recentSearchesTable];
}

-(void)pushSearchResult:(SearchResult*)results{
    ResultViewController *rvc = [[ResultViewController alloc] initWithResults:results Delegate:self];
    [self.navigationController pushViewController:rvc animated:YES];
}

#pragma mark - TableView Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == _suggestionTable){
        return _currentResult.suggestedTitles.count;
    } else if (tableView == _recentSearchesTable) {
        return _recentSearches.count;
    }
    return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _suggestionTable) {
    
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"suggestion"];
        
        if (!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"suggestion"];
        }
        
        SuggestedTitle *st = [_currentResult.suggestedTitles objectAtIndex:indexPath.row];
        
        cell.textLabel.text = st.title;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"(%@)", st.year];
        return cell;

    } else if (tableView == _recentSearchesTable){
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"recent"];
        
        if (!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"recent"];
        }
        
        SearchResult *sr = [_recentSearches objectAtIndex:indexPath.row];
        
        cell.textLabel.text = sr.title;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"(%@)",sr.year];
        [cell.imageView setImage:sr.image];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
    }
    return nil;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
    [label applySectionHeaderStyle];
    
    if (tableView == _suggestionTable) {
        label.text = @"  Did you mean..?";
    } else {
        label.text = @"  Recent Searches";
    }
    return label;
}

#pragma mark - TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _suggestionTable){
        return 44;
    } else if (tableView == _recentSearchesTable){
        return 80;
    }
    return 10;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    if (tableView == _suggestionTable){
        SuggestedTitle *st = [_currentResult.suggestedTitles objectAtIndex:indexPath.row];
        [self initiateSearchWithTitle:st.title Year:st.year];
        
        _searchField.text = [NSString stringWithFormat:@"%@",st.title];
        
        [tableView removeFromSuperview];
        tableView = nil;
        
    } else if (tableView == _recentSearchesTable){
        
        SearchResult *sr = [_recentSearches objectAtIndex:indexPath.row];
        [self pushSearchResult:sr];
    }
}

@end
