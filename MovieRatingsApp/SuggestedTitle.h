//
//  SuggestedTitle.h
//  MovieRatingsApp
//
//  Created by Niko on 3/1/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SuggestedTitle : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *year;

+(NSArray*)dummyArray;

@end
