//
//  SuggestedTitle.m
//  MovieRatingsApp
//
//  Created by Niko on 3/1/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "SuggestedTitle.h"

@implementation SuggestedTitle

+(NSArray*)dummyArray{
    
    NSMutableArray *incSuggestions = [[NSMutableArray alloc] initWithCapacity:4];
    
    SuggestedTitle *st1 = [[SuggestedTitle alloc] init];
    st1.title = @"The Big Lebowski";
    st1.year = @"1234";
    [incSuggestions addObject:st1];
    
    SuggestedTitle *st2 = [[SuggestedTitle alloc] init];
    st2.title = @"Pulp Fiction";
    st2.year = @"2355";
    [incSuggestions addObject:st2];
    
    SuggestedTitle *st3 = [[SuggestedTitle alloc] init];
    st3.title = @"Easy Rider";
    st3.year = @"7543";
    [incSuggestions addObject:st3];
    
    SuggestedTitle *st4 = [[SuggestedTitle alloc] init];
    st4.title = @"The Royal Tenenbaums";
    st4.year = @"9999";
    [incSuggestions addObject:st4];
    
    return incSuggestions;
}

-(NSString*)description{
    
    return [NSString stringWithFormat:@"Suggestion: %@ %@", _title, _year];
}

@end
