//
//  UIImage+Extras.h
//  MovieRatingsApp
//
//  Created by Niko on 7/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extras)
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
