//
//  UIImage+Extras.m
//  MovieRatingsApp
//
//  Created by Niko on 7/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "UIImage+Extras.h"

@implementation UIImage (Extras)
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    // Create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);   // Fill it with your color
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
