//
//  UILabel+Extras.h
//  MovieRatingsApp
//
//  Created by Niko on 7/28/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extras)

-(void)applySectionHeaderStyle;

@end
