//
//  UILabel+Extras.m
//  MovieRatingsApp
//
//  Created by Niko on 7/28/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "UILabel+Extras.h"
#import "ViewHelpers.h"

@implementation UILabel (Extras)

-(void)applySectionHeaderStyle{
    self.textColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor darkGrayColor];
    self.font = [UIFont fontWithName:HELVETICA_NEUE_LIGHT size:16];
}

@end
