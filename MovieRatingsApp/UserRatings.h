//
//  UserRatings.h
//  MovieRatingsApp
//
//  Created by Niko on 4/7/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserRatings : NSManagedObject

@property (nonatomic, retain) NSString * userRatingID;
@property (nonatomic, retain) NSString * userID;
@property (nonatomic, retain) NSString * imdbID;
@property (nonatomic, retain) NSString * rating;

@end
