//
//  UserRatings.m
//  MovieRatingsApp
//
//  Created by Niko on 4/7/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "UserRatings.h"


@implementation UserRatings

@dynamic userRatingID;
@dynamic userID;
@dynamic imdbID;
@dynamic rating;

@end
