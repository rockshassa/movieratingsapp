//
//  ViewHelpers.h
//  MovieRatingsApp
//
//  Created by Niko on 7/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Extras.h"
#import "UILabel+Extras.h"

#define HELVETICA_NEUE @"HelveticaNeue"
#define HELVETICA_NEUE_BOLD @"HelveticaNeue-Bold"
#define HELVETICA_NEUE_LIGHT @"HelveticaNeue-Light"

static CGFloat bottomOfView(UIView *v)
{
    if (v) {
        return ceilf(v.frame.origin.y + v.frame.size.height);
    } else {
        return 0;
    }
}

static UIButton* buttonWithColor(UIColor*color){
    UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
    [b setBackgroundImage:[UIImage imageWithColor:color] forState:UIControlStateNormal];
    b.titleLabel.font = [UIFont fontWithName:HELVETICA_NEUE_LIGHT size:20];
    b.titleLabel.textColor = [UIColor whiteColor];
    b.layer.cornerRadius = 10;
    b.clipsToBounds = YES;
    
    return b;
}