//
//  main.m
//  MovieRatingsApp
//
//  Created by Niko on 2/24/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NGAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        
        @try {
            return UIApplicationMain(argc, argv, nil, NSStringFromClass([NGAppDelegate class]));
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception.callStackSymbols);
        }
    }
}
